var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
    return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(uporabnik, callback) {
    ehrId = "";
    $.ajaxSetup({
        headers: {
            "Authorization": getAuthorization()
        }
    });
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function (data) {
            var ehrId = data.ehrId;
            console.log(ehrId);
            var partyData = {
                "firstNames": uporabnik.firstName,
                "lastNames": uporabnik.lastName,
                "dateOfBirth": uporabnik.dateOfBirth,
                "additionalInfo": {
                    "ehrId": ehrId
                }
            };
            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                headers: {
                    "Authorization": getAuthorization()
                },
                contentType: 'application/json',
                data: JSON.stringify(partyData),
                success: function (party) {
                    if (party.action == 'CREATE') {
                        $.ajax(baseUrl + "/composition?" + $.param({
                            "ehrId": ehrId,
                            templateId: "Vital Signs",
                            format: "FLAT",
                            comitter: "ME"
                        }), {
                            type: "POST",
                            contentType: "application/json",
                            headers: {
                                "Authorization": getAuthorization()
                            },
                            data: JSON.stringify({
                                "ctx/language": "en",
                                "ctx/territory": "SI",
                                "ctx/time": uporabnik.data.dateTime,
                                "vital_signs/height_length/any_event/body_height_length": uporabnik.data.height,
                                "vital_signs/body_weight/any_event/body_weight": uporabnik.data.weight,
                                "vital_signs/body_temperature/any_event/temperature|magnitude": uporabnik.data.temperature,
                                "vital_signs/body_temperature/any_event/temperature|unit": "°C",
                                "vital_signs/blood_pressure/any_event/systolic": uporabnik.data.systolic,
                                "vital_signs/blood_pressure/any_event/diastolic": uporabnik.data.diastolic,
                                "vital_signs/indirect_oximetry:0/spo2|numerator": uporabnik.data.oxygen
                            }),
                            success: function () {
                                console.log("Ustvaril uporabnika");
                            }
                        });
                    }
                    callback(ehrId);
                }
            });
        }
    });
}

function dodajPodatke(uporabnik) {
    $.ajax(baseUrl + "/composition?" + $.param({
        "ehrId": uporabnik.ehrid,
        templateId: "Vital Signs",
        format: "FLAT",
        comitter: "ME"
    }), {
        type: "POST",
        contentType: "application/json",
        headers: {
            "Authorization": getAuthorization()
        },
        data: JSON.stringify({
            "ctx/language": "en",
            "ctx/territory": "SI",
            "ctx/time": uporabnik.data.dateTime,
            "vital_signs/height_length/any_event/body_height_length": uporabnik.data.height,
            "vital_signs/body_weight/any_event/body_weight": uporabnik.data.weight,
            "vital_signs/body_temperature/any_event/temperature|magnitude": uporabnik.data.temperature,
            "vital_signs/body_temperature/any_event/temperature|unit": "°C",
            "vital_signs/blood_pressure/any_event/systolic": uporabnik.data.systolic,
            "vital_signs/blood_pressure/any_event/diastolic": uporabnik.data.diastolic,
            "vital_signs/indirect_oximetry:0/spo2|numerator": uporabnik.data.oxygen
        }),
        success: function () {
            console.log("Dodal podatke");
        }
    });
}

function pridobiPodatke(ehrid, callback) {
    $.ajax(baseUrl + "/demographics/ehr/" + ehrid + "/party", {
        type: "GET",
        headers: {
            "Authorization": getAuthorization()
        },
        success: function (partyData) {
            $.ajax(baseUrl + "/view/" + ehrid + "/height", {
                type: "GET",
                headers: {
                    "Authorization": getAuthorization()
                },
                success: function (heightData) {
                    $.ajax(baseUrl + "/view/" + ehrid + "/weight", {
                        type: "GET",
                        headers: {
                            "Authorization": getAuthorization()
                        },
                        success: function (weightData) {
                            $.ajax(baseUrl + "/view/" + ehrid + "/blood_pressure", {
                                type: "GET",
                                headers: {
                                    "Authorization": getAuthorization()
                                },
                                success: function (pressureData) {
                                    $.ajax(baseUrl + "/view/" + ehrid + "/body_temperature", {
                                        type: "GET",
                                        headers: {
                                            "Authorization": getAuthorization()
                                        },
                                        success: function (temperatureData) {
                                            $.ajax(baseUrl + "/view/" + ehrid + "/spO2", {
                                                type: "GET",
                                                headers: {
                                                    "Authorization": getAuthorization()
                                                },
                                                success: function (oxygen) {
                                                    var info = {
                                                        "party": partyData,
                                                        "height": heightData,
                                                        "weight": weightData,
                                                        "pressure": pressureData,
                                                        "temperature": temperatureData,
                                                        "oxygen": oxygen
                                                    }
                                                    callback(info);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija


function drawGraph(dataset) {
    var svgWidth = 400,
        svgHeight = 300,
        barPadding = 5;

    var barWidth = 15;

    var svg = d3.select('svg')
        .attr("width", svgWidth)
        .attr("height", svgHeight)
        .attr("class", "bar-chart");

    var yScale = d3.scaleLinear()
        .domain([0, d3.max(dataset)])
        .range([svgHeight, 0]);

    var xScale = d3.scaleLinear()
        .domain([0, d3.max(dataset)])
        .range([0, svgWidth]);

    var y_axis = d3.axisLeft()
        .scale(yScale);

    var barChart = svg.selectAll("rect")
        .data(dataset)
        .enter()
        .append("rect")
        .attr("y", function (d) {
            return yScale(d);
        })
        .attr("height", function (d) {
            return svgHeight - yScale(d);
        })
        .attr("width", barWidth - barPadding)
        .attr("transform", function (d, i) {
            var translate = [barWidth * i + 40, 10];
            return "translate(" + translate + ")";
        });

    var borderPath = svg.append("rect")
        .attr("x", 0)
        .attr("y", 0)
        .attr("height", svgHeight)
        .attr("width", svgWidth)
        .style("stroke", "black")
        .style("fill", "none")
        .style("stroke-width", 5);

    svg.append("g")
        .attr("transform", "translate(30, 10)")
        .call(y_axis);

    svg.on("click", function () {
        var coords = d3.mouse(this);
        console.log(coords);

        svg.selectAll("rect") // For new circle, go through the update process
            .data(dataset)
            .enter()
            .append("rect")
    });
}